import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

declare var $:any;
declare var window:any;

import { Observable }     from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise'; 

@Injectable()
export class Services {

  constructor (private http: Http) {}

  getResource(route : string): Promise<any[]> {

    return Promise.resolve(this.http.get(route)
                  .toPromise()
                  .then((res) => JSON.parse(res.text()))
                  .catch((res) => []));
  }
}