import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare var $: any;
declare var window: any;
declare var WOW: any;
declare var Waves: any;
declare var map: any;

@Component({
    selector: 'div-dashboard',
    //templateUrl: '../../pages/dashboard/dashboard.html',
    templateUrl: 'http://172.16.2.255:3000/build/pages/dashboard/dashboard.html',
    providers: []
})


export class DashboardComponent implements OnInit {

    tabs: any;
    menu: Array<{ title: any, url: any, activeRoute: any, icon: any, submenu: any }>;

    constructor() {

        this.menu = [
            {
                title: 'Início',
                url: '/',
                activeRoute: false,
                icon: "home",
                submenu: false
            },
            {
                title: 'Consultas',
                url: 'javascript:void(0);',
                activeRoute: 'active',
                icon: 'search',
                submenu: [
                    {
                        title: 'Página',
                        url: '/dashboard/consulta/pagina',
                        activeRoute: false,
                        submenu: false
                    },
                    {
                        title: 'Perfil',
                        url: '/dashboard/consulta/perfil',
                        activeRoute: false,
                        submenu: false
                    },
                    {
                        title: 'Ação',
                        url: '/dashboard/consulta/acao',
                        activeRoute: false,
                        submenu: false
                    },
                    {
                        title: 'Orgão',
                        url: '/dashboard/consulta/orgao',
                        activeRoute: false,
                        submenu: false
                    },
                    {
                        title: 'Operador Externo',
                        url: '/dashboard/consulta/operador-externo',
                        activeRoute: false,
                        submenu: false
                    },
                    {
                        title: 'Operador Interno',
                        url: '/dashboard/consulta/operador-interno',
                        activeRoute: false,
                        submenu: false
                    }
                ]
            },
            {
                title: 'Autorização',
                url: 'javascript:void(0);',
                activeRoute: 'active',
                icon: 'shield',
                submenu: [
                    {
                        title: 'Perfil - Operador',
                        url: '/dashboard/autorizacao/perfil-operador',
                        activeRoute: false,
                        submenu: false
                    },
                    {
                        title: 'Perfil - Operador Externo',
                        url: '/dashboard/autorizacao/perfil-operador-externo',
                        activeRoute: false,
                        submenu: false
                    }
                ]
            }
        ];
    }

    ngOnInit(): void {
    }



    //Scripts jQuery
    ngAfterViewInit() {
        $(".selectpicker").selectpicker();
        $("#mobile-button").click(function(){
            if ($(this).hasClass('open')) {
                $(this).removeClass('open');
                $(".sidebar").removeClass('active');
                $('.overlay').remove();
                $("body").css({"overflow" : "visible"});
            } else {
                $(this).addClass('open');
                $(".sidebar").addClass('active');
                $('body').append('<div class="overlay"></div>');
                $("body").css({"overflow" : "hidden"});
                setTimeout(function(){
                    $(".overlay").addClass('black');
                }, 50);
                $('.overlay').click(function () {
                    $('#mobile-button').removeClass('open');
                    $('.sidebar').removeClass('active');
                    $("body").css({"overflow" : "visible"});
                    $(this).remove();
                });
            }
        });
        new WOW().init();
        Waves.attach('.waves-effect', ['waves-button']);
        Waves.init();

        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar .menu .dropdown").click(function () {
            $(this).find("ul").slideToggle('fast');
        });

        $("#open").click(function () {
            $(".content").addClass('active');
            $("#mesa-eletronica").fadeIn();
            new WOW().init();
        });
        $("#close").click(function () {
            $(".content").removeClass('active');
            $("#mesa-eletronica").fadeOut();
            new WOW().init();
        });

        $('form .form-control').each(function () {
            if ($(this).val() !== '') {
                 $(this).parent().addClass('active');
            }
        });
        $('form .form-control').focus(function () {
            $(this).parent().addClass('active');
        });

        $('form .form-control').blur(function () {
            if ($(this).val() === '') {
                $(this).parent().removeClass('active');
            }
        });
        $('.drop-alert').click(function () {
            if ($('.drop-alert').hasClass('active')) {
                $(this).removeClass('active');
                $(this).find('ul').removeClass('open');
                $('.overlay').remove();
            } else {
                $(this).addClass('active');
                $(this).find('ul').addClass('open');
                $('body').append('<div class="overlay"></div>');
                $('.overlay').click(function () {
                    $('.drop-alert').removeClass('active');
                    $('.drop-alert ul').removeClass('open');
                    $(this).remove();
                });
            }
        });

        $('.single-date').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });

        $('.single-date').on('show.daterangepicker', function (ev: any, picker: any) {
            Waves.attach('.daterangepicker.single.dropdown-menu .table-condensed tr th.next', ['waves-button']);
            Waves.attach('.daterangepicker.single.dropdown-menu .table-condensed tr th.prev', ['waves-button']);
            Waves.attach('.daterangepicker.single.dropdown-menu .table-condensed tr td', ['waves-button']);
            Waves.init();
        });
        $('.single-date').on('showCalendar.daterangepicker', function (ev: any, picker: any) {
            Waves.attach('.daterangepicker.single.dropdown-menu .table-condensed tr th.next', ['waves-button']);
            Waves.attach('.daterangepicker.single.dropdown-menu .table-condensed tr th.prev', ['waves-button']);
            Waves.init();
        });
        $('.date').daterangepicker();

        $('.modal').on('show.bs.modal', function (e: any) {
            $(".content").addClass('active');
            $(".btn-floating").addClass('animated fadeOutRight');
            $(".btn-floating").removeClass('fadeInLeft');
        });

        $('.modal').on('hidden.bs.modal', function (e: any) {
            $(".content").removeClass('active');
            $(".btn-floating").removeClass('fadeOutRight');
            $(".btn-floating").addClass('animated fadeInLeft');
        });

        $("#sistemaOpen").click(function () {
            $("#sistema").modal('show');
        });
        $(".btn-floating").click(function () {
            if ($('.btn-floating').hasClass('active')) {
                $(this).removeClass('active');
                setTimeout(function () {
                    $('.btn-floating').css({ 'overflow': 'hidden' });
                }, 800);
                $('.overlay').remove();
                $(".btn-floating a.left").css({ "right": 0 });
            } else {
                $(this).addClass('active');
                $('body').append('<div class="overlay"></div>');

                $('.btn-floating').css({ 'overflow': 'visible' });

                $(".btn-floating a.left").each(function (key: any, value: any) {
                    var total = 65 * (key + 1);
                    $(value).css("right", total + "px");
                });



                $('.overlay').click(function () {
                    $('.btn-floating').removeClass('active');
                    $(this).remove();
                    $(".btn-floating a.left").css({ "right": 0 });
                });
            }
        });

        $('.btn-floating').waypoint(function (direction: any) {
            if (direction === 'down') {
                // Do stuff
                $(".btn-floating").css({ "bottom": "20px" });

            } else {
                // Do stuff
                $(".btn-floating").css({ "bottom": "-70px" });
            }
        }, {
                offset: '100%'
            });

    }
}