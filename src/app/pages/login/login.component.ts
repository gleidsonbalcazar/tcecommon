import { Component, OnInit } from '@angular/core';
import { Router }   from '@angular/router';


declare var $:any;
declare var WOW:any;
declare var Waves:any;

@Component({
  selector: 'div-login',
  templateUrl: 'http://172.16.2.255:3000/build/pages/login/login.html',
  providers: []
})

export class LoginComponent implements OnInit {
  ngOnInit(): void {}
  ngAfterViewInit() {
    new WOW().init();
    Waves.attach('.waves-effect', ['waves-button']);
    Waves.init();

    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('input').iCheck({
        checkboxClass: "icheckbox_flat",
        radioClass: "iradio_flat"
    });
  }
}