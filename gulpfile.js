var gulp = require('gulp');
var ts = require('gulp-typescript');
var tsConfig = require('./tsconfig.json');
var connect = require('gulp-connect');
var sourcemaps = require('gulp-sourcemaps');
var g = require('gulp-load-plugins')();

//Typescript Config;
var tsProject = ts.createProject(tsConfig.compilerOptions);

//compile app typescript files
gulp.task('compile:app', function(){
  return gulp.src('src/**/*.ts')
    .pipe(ts(tsProject))
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write(".", {sourceRoot: '/src'}))
    .pipe(gulp.dest('./build'))
    .pipe(connect.reload());
});

//copy js files
gulp.task('copy:src', function(){
  return gulp.src([
    'src/*js',
    'src/**/*css',
    'src/**/*html'
  ])
  .pipe(gulp.dest('build'))
  .pipe(connect.reload());
});


//live reload server
gulp.task('server', ['copy:src','compile:app'], function() {
  connect.server({
    root: 'build',
    livereload: true
  });
});

//default task
gulp.task('default', ['server'], function(){
  gulp.watch(['src/**/*.ts'], ['compile:app']);
  gulp.watch(['src/**/.js'], ['copy:src']);
});

